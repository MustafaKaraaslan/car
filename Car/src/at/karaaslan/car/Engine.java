package at.karaaslan.car;

public class Engine {
	
	private String engine;
	private String power;
	
	
	public Engine(String engine, String power) {
		super();
		this.engine = engine;
		this.power = power;
	}


	public String getEngine() {
		return engine;
	}


	public void setEngine(String engine) {
		this.engine = engine;
	}


	public String getPower() {
		return power;
	}


	public void setPower(String power) {
		this.power = power;
	}
	
	

}
