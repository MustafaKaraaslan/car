package at.karaaslan.car;

public class Manufacturer {
	private String name;
	private String localPlace;
	private int discount;
	
	
	public Manufacturer(String name, String localPlace, int discount ) {
		super();
		this.name = name;
		this.localPlace = localPlace;
		this.discount = discount;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLocalPlace() {
		return localPlace;
	}


	public void setLocalPlace(String localPlace) {
		this.localPlace = localPlace;
	}


	public int getDiscount() {
		return discount;
	}


	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
	
	
}
