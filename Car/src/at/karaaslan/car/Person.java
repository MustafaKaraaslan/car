package at.karaaslan.car;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class Person {
	private String fName;
	private String lName;
	private LocalDate birthday;
	private List<Car> cars;
	
	
	public Person(String fName, String lName, LocalDate birthday) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.birthday = birthday;
		this.cars = new ArrayList<>();
	}
	
	public void addCar(Car caar) {
		this.cars.add(caar);
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	public int valueCars() {
		int amounth = 0;
		for (Car car : cars) {
			amounth = amounth + car.getPrice();
		}
		return amounth;
	}
}

